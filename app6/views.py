from django.shortcuts import render,redirect
from django.http import Http404, HttpResponse
from .models import Status
from .forms import Status_Forms
from datetime import datetime

# Create your views here.
def index(request):
	if request.method == "POST":
		form = Status_Forms(request.POST)
		if form.is_valid():
			model_instance = Status(
				status = form.data['status'],
    waktu = datetime.now(),
			)
			model_instance.save()
			return redirect("/")
				
	else:
		form = Status_Forms()
		
	isi = Status.objects.all().order_by('-waktu')
	argument={
		'form': form,
  		'data': isi
	}


	return render(request, "index.html",argument)	