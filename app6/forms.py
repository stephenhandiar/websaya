from django import forms
from .models import Status
from django.forms import widgets

class Status_Forms(forms.Form):
 status = forms.CharField(
  label = 'Status  ', max_length=300, required = True,
  widget = forms.TextInput(
            attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Masukkan Status kamu Saat ini",
            }),
 )
class meta:
 model = Status
 field =['status', 'waktu']