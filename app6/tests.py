from django.test import TestCase
from django.urls import resolve,reverse
from django.apps import apps
from app6.apps import App6Config
from .models import Status
from .views import index
from .forms import Status_Forms
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class StorySixUnitTest(TestCase):

     def test_landing_urls_is_exist(self):
          response = self.client.get('/')
          self.assertEqual(response.status_code, 200)
        
     def test_landing_using_index_tempalets(self):
          response = self.client.get('/')
          self.assertTemplateUsed(response, 'index.html')

     def test_landing_use_correct_views(self):
        target = resolve('/')
        self.assertEqual(target.func, index)

     def create_status(self):
        new_status = Status.objects.create(status = "Ini adalah sebuat status.")
        return new_status

     def test_check_status(self):
        c = self.create_status()
        self.assertTrue(isinstance(c, Status))
        self.assertTrue(c.__str__(), c.status)
        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

     def test_apps(self):
        self.assertEqual(App6Config.name, 'app6')
        self.assertEqual(apps.get_app_config('app6').name, 'app6')

     def test_form(self):
        form_data = {
        'status' : 'ini adalah sebuah status',

        }
        form = Status_Forms(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

class StorySixFunctionalTest(TestCase):
   
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StorySixFunctionalTest, self).setUp()

    def tearDown(self):
        time.sleep(5)
        self.selenium.quit()
        super(StorySixFunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_name('submit')

        status.send_keys("Coba Coba")
        time.sleep(5)
        submit.send_keys(Keys.RETURN)
        time.sleep(5)
      